#!/usr/bin/python3.6

import requests # HTTP Python Library (https://github.com/requests/requests_

coloredExists = False

try:
    from colored import fg, attr # Color Python Library for Terminal (https://github.com/dslackw/colored)
    coloredExists = True
except:
    print("Warning: colored is not installed!\nTry: pip3 install colored\n") # If colored Library not installed on System
    # Show Error message(up line) for user

if coloredExists:
    OKColor = fg('#008000') # Green Foreground Color
    NOTColor = fg('#FF0000') # Red Foreground Color
    MovColor = fg('#FFFF00') # Yellow Foreground Color
    ResColor = attr('reset') # Reset Color
else:
    OKColor = ""
    NOTColor = ""
    MovColor = ""
    ResColor = ""

print ("this action maybe take a few minutes...\n\n") # Show Message in Terminal for user

File = open('site.list', 'r') # Open File site.list 
site_list = File.readlines() # Read site.list lines
site_list = [k.replace("\n", "") for k in site_list] # remove \n in lines
File.close() # site.list file closed
session = requests.Session() 
session.trust_env = False # Disable System Proxy

for url in site_list:

    http_url = 'http://' + url
    https_url= 'https://'+ url
    http_requests = session.get(http_url)

    #http req part
    http_report = 'Name: '+ http_url +'\nStatus: '+ str(http_requests) + '\nHeaders: '+ str(http_requests.headers)+'\n\n'
    report =open('report.log','a')
    report.writelines(http_report)
    report.close()
    if str(http_requests)  == "<Response [200]>":
        print ( http_url + ":" + OKColor + " OK!" + ResColor)
    elif str(http_requests) == "<Response [301]>":
        print ( http_url + ":" + MovColor + " Moved Permanently!" + ResColor)
    else:
        print ( http_url + ":" + NOTColor + " NOT!" + ResColor)

    #https req part
    try:
        https_requests = session.get(https_url , timeout=7)
    except requests.exceptions.RequestException:
        https_report = "Name: " + https_url +'\nStatus: ' + 'Https Block\n\n'
        report =open('report.log','a')
        report.writelines(https_report)
        report.close()
        print( https_url + ":" + NOTColor + "NOT!" + ResColor)
    else:
        https_report = 'Name: '+ https_url +'\nStatus: '+ str(https_requests) + '\nHeaders: '+ str(https_requests.headers)+'\n\n'
        report = open('report.log', 'a')
        report.writelines(https_report)
        report.close()
        if str(https_requests) == "<Response [200]>":
            print (https_url + ":" + OKColor + " OK!" + ResColor)
        elif  str(https_requests) == "<Response [301]>":
            print (https_url + ":" + MovColor + " Moved Permanently!" + ResColor)
        else:
            print (https_url + ":" + NOTColor + " NOT!" + ResColor)
print ("\nDone!")

